#!/usr/bin/env python
# -*- coding: utf8 -*-

'''
finder - find satellites appearing within a certain timeframe at a given location

Copyleft 2018 GPLv3 - Jogi Hofmüller <jogi@mur.at>
'''

import ephem
from datetime import datetime, timedelta
from OSCSatellite import Config, Satellite, TLEs

# satellites with AOS within riseframe minutes
riseframe = timedelta(minutes = 5)

# satellites with LOS within setframe minutes
setframe = timedelta(minutes = 14)

if __name__ == '__main__':
    concertconfig = open('concert.cfg', 'w')
    config = Config()
    config.set_variables()
    tles = TLEs(config)
    observer = ephem.Observer()
    observer.lon, observer.lat, observer.elevation = config.longitude, config.latitude, config.altitude
    utcnow = datetime.utcnow() + timedelta(seconds = 10)
    observer.date = utcnow.strftime('%Y/%m/%d %H:%M:%S.%f')
    satellites = []

    counter = 0
    # record the lowest rise_time and the largest set_time
    firstrise = utcnow + riseframe
    lastset = utcnow
    hma = ephem.degrees(0)
    hma_sat = ''

    for line in tles.tles:
        satellite = Satellite(line[1], observer)
        satellite.update()

        try:
            rt, rise_azimuth, max_alt_time, max_alt, st, set_azimuth = observer.next_pass(satellite.sat)
        except ValueError:
            continue

        if not st:
            continue

        skip = False
        for s in satellites:
            if s.sat.catalog_number == satellite.sat.catalog_number:
                skip = True

        if skip:
            print('skipping duplicate satellite {}'.format(satellite.sat.catalog_number))
            continue

        rise_time = datetime.strptime(str(rt), '%Y/%m/%d %H:%M:%S')
        set_time = datetime.strptime(str(st), '%Y/%m/%d %H:%M:%S')

        if rise_time > utcnow and (rise_time - utcnow) < riseframe and (set_time - utcnow) < setframe:
            print('{:2}:  {} - {}\nvon {}\nbis {}\nin {}\nmax alt {}\n'.format(
                counter + 1,
                satellite.sat.catalog_number,
                satellite.tle[0],
                satellite.sat.rise_time,
                satellite.sat.set_time,
                rise_time - utcnow,
                max_alt))
            counter += 1
            if rise_time < firstrise:
                firstrise = rise_time
            if set_time > lastset:
                lastset = set_time
            if max_alt > hma:
                hma = max_alt
                hma_name = '{} - {}'.format(satellite.sat.catalog_number, satellite.tle[0])
            satellites.append(satellite)

    print('Start:  {} UTC\n  End:  {} UTC\n  HMA:  {}\n'.format(firstrise, lastset, hma))

    uris = ''
    for uri in config.tle_uris:
        uris = '{}{} '.format(uris, uri)

    numbers = ''
    for sat in satellites:
        numbers = '{}{} '.format(numbers, sat.sat.catalog_number)

    configtext = '''# automatically generated config file
[global]
address = 127.0.0.1
port = 4242
limit_to_satellites = {numbers}
exclude_satellites = 
resolution = 0.1
tle_file_name = tle.txt
tle_uris = {uris}
auto_update = False
update_interval = 6
[observer]
longitude = 15.42
latitude = 47.05
altitude = 350
[data]
alt = elevation
az = azimuth
elevation = altitude
range = range
range_velocity = velocity
    '''
    concertconfig.write(configtext.format(numbers = numbers, uris = uris))

    pdpatch = open('osc-satellite.pd', 'r').read()
    pdpatch = '{}#X text 30 240 Start: {} UTC;\n'.format(pdpatch, firstrise)
    pdpatch = '{}#X text 49 260 End: {} UTC;\n'.format(pdpatch, lastset)
    pdpatch = '{}#X text 49 280 HMA: {} deg by {};\n'.format(pdpatch, hma, hma_name)

    yoffset = 320
    counter = 1
    for sat in sorted(satellites, key=lambda s: s.sat.rise_time):
        if counter % 2 == 0:
            pdpatch = '{}#X obj 30 {} sat-high {};\n'.format(pdpatch, yoffset, sat.sat.catalog_number)
        else:
            pdpatch = '{}#X obj 30 {} sat-low {};\n'.format(pdpatch, yoffset, sat.sat.catalog_number)
        pdpatch = '{}#X text 290 {} {:2}: satellite {} - {};\n'.format(
                pdpatch, yoffset, counter, sat.sat.catalog_number, sat.tle[0])
        yoffset += 36
        counter += 1

    concertpatch = open('this-concert.pd', 'w')
    concertpatch.write(pdpatch)
