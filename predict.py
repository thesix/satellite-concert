#!/usr/bin/env python
# -*- coding: utf8 -*-

'''
finder - find satellites appearing within a certain timeframe at a given location

Copyleft 2018 GPLv3 - Jogi Hofmüller <jogi@mur.at>
'''

import ephem
from datetime import datetime, timedelta
from OSCSatellite import Config, Satellite, TLEs

if __name__ == '__main__':
    config = Config()
    config.set_variables()
    tles = TLEs(config)
    observer = ephem.Observer()
    observer.lon, observer.lat, observer.elevation = config.longitude, config.latitude, config.altitude

    # satellites with AOS within timeframe seconds
    timeframe = timedelta(minutes=30)
    # starting at which time (UTC)?
    utctime = datetime.utcnow().replace(year=2018,month=8,day=13,hour=9,minute=17)
    observer.date = utctime.strftime('%Y/%m/%d %H:%M:%S.%f')
    satellites = []

    # record the lowest rise_time and the largest set_time
    firstrise = utctime + timeframe
    lastset = utctime
    hma = ephem.degrees(0)
    hma_sat = ''

    for line in tles.tles:
        satellite = Satellite(line[1], observer)
        satellite.update()
        try:
            rt, rise_azimuth, max_alt_time, max_alt, st, set_azimuth = observer.next_pass(satellite.sat)
        except ValueError:
            continue

        if not st:
            continue

        rise_time = datetime.strptime(str(rt), '%Y/%m/%d %H:%M:%S')
        set_time = datetime.strptime(str(st), '%Y/%m/%d %H:%M:%S')

        if rise_time > utctime and (rise_time - utctime) < timeframe:
            satellite.max_alt = max_alt
            satellite.rise_time = rise_time
            satellites.append(satellite)
            if rise_time < firstrise:
                firstrise = rise_time
            if set_time > lastset:
                lastset = set_time
            if max_alt > hma:
                hma = max_alt
                hma_name = '{} - {}'.format(satellite.sat.catalog_number, satellite.tle[0])

    counter = 1
    for sat in sorted(satellites, key=lambda s: s.rise_time):
        print('{:2}:  {} - {}\nvon {}\nbis {}\nin {}\nmax alt {}\n'.format(
            counter,
            sat.sat.catalog_number,
            sat.tle[0],
            sat.sat.rise_time,
            sat.sat.set_time,
            sat.rise_time - utctime,
            sat.max_alt))
        counter += 1

    print('Start:  {} UTC\n  End:  {} UTC\n  HMA:  {}\n'.format(firstrise, lastset, hma))

