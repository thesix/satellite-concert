# Satellite Concert

Satellite Concert is a set of PD patches and scripts that I use to perform a
concert using tracking data from satellites.  It uses OSCSatellite, an osc
application I wrote to transform satellite's positional data to osc.  You
can get it from it's [gitlab page](//gitlab.com/thesix/OSCSatellite).

To use it you first have to start an instance of OSCSatellite that you
configure with the classes of satellites you want to use.

The two scripts `finder.py` and `predict.py` also need OSCsatellite
installed somewhere in your python path (or in this directory).  You should
edit `finder.cfg` in order to get the data according to you current
location.

Once you have all things in place it should suffice to run `./startall`.
You will first get a list of satellites that will appear above the horizon
of your current position within the next five minutes.  If you accept the
list (by hitting enter) the script will create a PD patch for the concert,
start OSCsatellite with the generated `concert.cfg`, wait a few seconds and
then start pd running the `this-concert.pd` patch.
